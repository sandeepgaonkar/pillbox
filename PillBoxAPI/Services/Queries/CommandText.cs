﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PillBoxAPI.Services.Queries
{
    public interface ICommandText
    {
        string GetPatients { get; }
        string GetPatientById { get; }
        string AddPatient { get; }
        string UpdatePatient { get; }
        string RemovePatient { get; }
    }
    public class CommandText : ICommandText
    {
        public string GetPatients => "Select * From Public.IOT_master_table_v3";
        public string GetPatientById => "Select start_date,end_date,morning_time, evening_time, night_time  From Public.IOT_master_table_v3 Where device_id=@Id";

        // public string GetPatientById => "Select start_date,end_date,date_part('hour', morning_time) : date_part('second', morning_time) : date_part('millisecond', morning_time) as morning_time  From Public.IOT_master_table_v3 Where device_id=@Id";
        public string AddPatient => "Insert Into  Public.IOT_master_table_v3 (device_id, user_id, start_date, end_date, morning_time, evening_time, night_time) " +
            "Values (@device_id, @user_id, @start_date,@end_date,@morning_time,@evening_time,@night_time)";
        public string UpdatePatient => "Update Public.IOT_master_table_v3 set Patient_Name = @Name, Cost = @Cost, CreatedDate = GETDATE() Where Id =@Id";
        public string RemovePatient => "Delete From Public.IOT_master_table_v3 Where Id= @Id";
        //public string GetPatients => "Select * From Patient";
        //public string GetPatientById => "Select * From public.IOT_Patient_details Where Patient_Id= @Id";
        //public string AddPatient => "Insert Into  public.IOT_Patient_details (Name, Cost, CreatedDate) Values (@Name, @Cost, @CreatedDate)";
        //public string UpdatePatient => "Update public.IOT_Patient_details set Patient_Name = @Name, Cost = @Cost, CreatedDate = GETDATE() Where Id =@Id";
        //public string RemovePatient => "Delete From Patient Where Id= @Id";
    }
}
