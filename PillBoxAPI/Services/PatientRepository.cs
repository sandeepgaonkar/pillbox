﻿using System;
using System.Data;
using System.Collections.Generic;
using Npgsql;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using PillBoxAPI.Services.Queries;
using System.Data.Common;

namespace PillBoxAPI.Services
{
    public class PatientRepository : IPatientRepository
    {
        private readonly ICommandText _commandText;
        private readonly string _connStr;
        public PatientRepository(IConfiguration configuration, ICommandText commandText)
        {
            _commandText = commandText;
            _connStr = configuration.GetConnectionString("Dapper");
        }
        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(_connStr);
            }
        }

        public List<Patient> GetAllPatients()
        {
            var query = ExecuteCommand(_connStr,
                   conn => conn.Query<Patient>(_commandText.GetPatients)).ToList();
            return query;
        }
        public Patient GetById(string id)
        {
            DateTime dt = DateTime.Parse("08:00:00");
            var Patient = ExecuteCommand<Patient>(_connStr, conn =>
                conn.Query<Patient>(_commandText.GetPatientById, new { @Id = id }).SingleOrDefault());
            
            return Patient;
        }
        public void AddPatient(Patient entity)
        {
            ExecuteCommand(_connStr, conn => {
                var query = conn.Query<Patient>(_commandText.AddPatient,
                    new { PatientName = entity.PatientName, StartDate = entity.Start_Date, EndDate = entity.End_Date, MorningTime = entity.Morning_Time1, EveningTime = entity.Evening_Time, NightTime = entity.Night_Time });
            });
        }
        public void UpdatePatient(Patient entity, int id)        {
            ExecuteCommand(_connStr, conn =>
            {
                var query = conn.Query<Patient>(_commandText.UpdatePatient,
                    new { PatientName = entity.PatientName, StartDate = entity.Start_Date, EndDate = entity.End_Date, PatientId = id });
            });
        }

        public void RemovePatient(int id)
        {
            ExecuteCommand(_connStr, conn =>
            {
                var query = conn.Query<Patient>(_commandText.RemovePatient, new { Id = id });
            });
        }


        #region Helpers

        private void ExecuteCommand(string connStr, Action<IDbConnection> task)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                task(dbConnection);
            }
            //    using (var conn = new DbConnection(connStr))
            //{
            //    conn.Open();

            //    task(conn);
            //}
        }
        private T ExecuteCommand<T>(string connStr, Func<IDbConnection, T> task)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return task(dbConnection); 
            }
            //using (var conn = new DbConnection(connStr))
            //{
            //    conn.Open();

            //    return task(conn);
            //}
        }
        #endregion
    }
}
