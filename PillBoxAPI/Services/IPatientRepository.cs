﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PillBoxAPI.Services
{
    public interface IPatientRepository
    {
        Patient GetById(String id);
        void AddPatient(Patient entity);
        void UpdatePatient(Patient entity, int id);
        void RemovePatient(int id);
        List<Patient> GetAllPatients();
    }
}
