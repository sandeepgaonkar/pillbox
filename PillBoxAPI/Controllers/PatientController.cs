﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PillBoxAPI.Services;

namespace PillBoxAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private readonly IPatientRepository _PatientRepository;

        public PatientController(IPatientRepository PatientRepository)
        {
            _PatientRepository = PatientRepository;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpGet]
        [Route("{id}")]
        public ActionResult<Patient> GetById(string id)
        {
            var Patient = _PatientRepository.GetById(id);
            return Ok(Patient);
        }
        [HttpPost]
        public ActionResult AddPatient(Patient entity)
        {
            _PatientRepository.AddPatient(entity);
            return Ok(entity);
        }
        [HttpPut("{id}")]
        public ActionResult<Patient> Update(Patient entity, int id)
        {
            _PatientRepository.UpdatePatient(entity, id);
            return Ok(entity);
        }
        [HttpDelete("{id}")]
        public ActionResult<Patient> Delete(int id)
        {
            _PatientRepository.RemovePatient(id);
            return Ok();
        }
    }
}
