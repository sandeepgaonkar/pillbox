﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PillBoxAPI
{
    
    public interface IPatient
    {
        Patient GetByPatientId(int id);
        void AddPatient(Patient entity);
        void UpdatePatient(Patient entity, int id);
        List<Patient> GetAllPatient();
    }
}
