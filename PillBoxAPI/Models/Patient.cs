﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PillBoxAPI
{
    public class Patient
    {
        private string _morning_Time;
        public int PatientId { get; set; }
        public int NurseId { get; set; }

        public string Device_Id { get; set; }
        public  string PatientName { get; set; }

        public int PatientAge { get; set; }

        public DateTime Start_Date { get; set; }

        public DateTime End_Date { get; set; }

        public TimeSpan Morning_Time { get; set; }
        public TimeSpan Evening_Time { get; set; }
        public TimeSpan Night_Time { get; set; }

       
    }
}

